<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

	Route::get('pdf/{id}','Api\RepairsController@pdf_gen');

	Route::get('products','Api\ProductsController@index');
	Route::put('products/updateproduct/{id}','Api\ProductsController@update');
	Route::get('products/delete/{id}','Api\ProductsController@setInactive');
	Route::post('products','Api\ProductsController@store');

	Route::get('users','Api\UserController@index');
	Route::get('users/check','Api\UserController@check');
    Route::post('users','Api\UserController@store');
    Route::get('users/delete/{id}','Api\UserController@destroy');
    Route::get('repairs/delete/{id}','Api\RepairsController@destroy');
    Route::put('users/newpassword/{id}','Api\UserController@update');

	Route::get('repairs','Api\RepairsController@index');
    Route::post('repairs','Api\RepairsController@store');
    Route::put('repairs/updaterepair/{id}','Api\RepairsController@update');
    Route::get('repairs/{number}','Api\RepairsController@show');
    Route::get('repairs/search/{number}','Api\RepairsController@searchRepair');
 

    
    Route::post('transaction','Api\TransactionsController@store');
    Route::post('login','Auth\LoginController@login');
    Route::post('logout','Auth\LoginController@logout'); 


