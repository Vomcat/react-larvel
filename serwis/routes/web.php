<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('welcome');
});
Route::get('/list', function () {
    return view('welcome');
})->middleware('auth');
Route::get('/home', function () {
    return view('welcome');
});
Route::get('/users', function () {
    return view('welcome');
})->middleware('auth')->middleware('role');

Route::get('/shop', function () {
    return view('welcome');
})->middleware('auth');

Route::get('/login', function () {
    return view('welcome');
});
Route::get('/products', function () {
    return view('welcome');
})->middleware('auth');
Route::get('pdf/{id}','Api\RepairsController@pdf_gen');





Route::resource('home','HomeController');











Auth::routes();