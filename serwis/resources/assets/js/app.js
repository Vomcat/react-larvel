
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes React and other helpers. It's a great starting point while
 * building robust, powerful web applications using React + Laravel.
 */

require('./bootstrap');

/**
 * Next, we will create a fresh React component instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

//require('./bootstrap');

//require('./components/Example');

import React from 'react'
import ReactDOM from 'react-dom'
import {  BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import Index from './components/index'
import Login from './components/login'
import Home from './components/home'
import List from './components/list'
import Stats from './components/stats'
import Users from './components/new_user'
import Shop from './components/shop'
import Products from './components/products'

ReactDOM.render(
	<Router>
	    <Switch>
    		<Route exact path='/' component={Index} />
			<Route path='/login' component={Login}/>
 			<Route path='/home' component={Home}/>
 			<Route path='/list' component={List}/>
 			<Route path='/stats' component={Stats}/>
 			<Route path='/users' component={Users}/>
 			<Route path='/shop' component={Shop}/>
 			<Route path='/products' component={Products}/>
		</Switch>
	</Router>,
    document.getElementById('app')
);
