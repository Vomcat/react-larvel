import React, { Component } from 'react'
import Nav from './navbar'
import axios from 'axios'
import Pagination from 'react-js-pagination'






class List extends Component {

  constructor(){
    super();
    this.state = {
      datatest:[], getvalue: null, m_edit: false,
       activePage:1,
       itemsCountPerPage:1,
       totalItemsCount:1,
       pageRangeDisplayed:3
    }
    this.handlePageChange=this.handlePageChange.bind(this);
  }

componentDidMount()
{
   axios.get('/api/repairs').then(response=>{
        this.setState({
        datatest: response.data.data,
        itemsCountPerPage:response.data.per_page,
        totalItemsCount:response.data.total,
        activePage:response.data.current_page
         }) 
  }).catch(error => {
        console.log(error)
      })
}

handlePageChange(pageNumber) {
    this.setState({activePage: pageNumber});
    axios.get('/api/repairs?page='+pageNumber).then(response=>{
        this.setState({
        datatest: response.data.data,
        itemsCountPerPage:response.data.per_page,
        totalItemsCount:response.data.total,
        activePage:response.data.current_page
         }) 
  }).catch(error => {
        console.log(error)
      })
  }


updateRepair(e){
        e.preventDefault();
        const {name , surname, phone_number, email, device, imei, description, code, cost, payment} = this.state ;
        axios.put('api/repairs/updaterepair/' + this.state.getvalue.id_r, {
            name, 
            surname,
            phone_number,
            email,
            device,
            imei,
            description,
            code,
            cost,
            payment
          })
          .then(response=> {
            this.setState({err: false});
            this.closeModal();
            this.componentWillMount();

            
          })
          .catch(error=> {
            this.refs.name.value="";
            this.setState({err: true});
          });
     }

deleteRepair(e){
        const {id} = this.state ;
        axios.get('api/repairs/delete/' + e.target.id).then(response=> {
            this.setState({err: false});
            this.componentWillMount();
            
          })
          .catch(error=> {
            this.setState({err: true});
          });
     }

searchRepair(e){
        e.preventDefault();
        const {number_repairs} = this.state ;
        
        axios.get('api/repairs/search/'+ number_repairs)
          .then(response=> {
            this.setState({ datatest: response.data}) 
                        
          })
          .catch(error=> {
             //this.setState({n_error: true, show : false}) 
            this.setState({err: true});
          });
     }

  loadModal(e){
    this.setState({m_edit: true, getvalue: this.state.datatest[e.target.id]});
    this.renderModal();
    

  }
  closeModal(){
    this.setState({m_edit: false});
    this.renderModal();
  }

  onChange(e){
    const {name, value} = e.target;
   
    console.log(this.state.datatest.length);
    this.setState({[name]: value});
    
        if(name == 'number_repairs')
        {
          if(value == ''){
            this.componentWillMount();
          }
          
        }
  }

  getPDFlink(){
    return(
      "pdf/"+ this.state.getvalue.id_r
      );
  }


 renderModal(){
  if(this.state.m_edit){
    return(
      <div>  
          <div className="container">
            <form role="form">
              <div className="form-row">
                <div className="form-group col-md-6">

                  <label htmlFor="name">Imie</label>
                  <input type="text" className="form-control" name="name"  id="name" placeholder="Imie" onChange={this.onChange.bind(this)} defaultValue={this.state.getvalue.name} required/>
                </div>
                <div className="form-group col-md-6">
                  <label htmlFor="surname">Nazwisko</label>
                  <input type="text" className="form-control" name="surname" id="surname" placeholder="Nazwisko" onChange={this.onChange.bind(this)} defaultValue={this.state.getvalue.surname} required />
                </div>
              </div>
              <div className="form-row">
                <div className="form-group col-md-6">
                  <label htmlFor="inputEmail4">Numer telefonu</label>
                  <input type="text" className="form-control" name="phone_number" id="phone_number" placeholder="5003003300" onChange={this.onChange.bind(this)} defaultValue={this.state.getvalue.phone_number} required />
                </div>
                <div className="form-group col-md-6">
                  <label htmlFor="email">Email</label>
                  <input type="text" className="form-control" name="email" id="email" placeholder="example@emial.com" onChange={this.onChange.bind(this)} defaultValue={this.state.getvalue.email} required />
                </div>
              </div>

              <div className="form-row">
                <div className="form-group col-md">
                  <label htmlFor="inputEmail4">Nazwa urządzenia</label>
                  <input type="text" className="form-control"  name="device" id="inputEmail4" placeholder="Nazwa" onChange={this.onChange.bind(this)} defaultValue={this.state.getvalue.device} required/>
                </div>
              </div>
              <div className="form-row">
                <div className="form-group col-md">
                  <label htmlFor="inputEmail4">Numer seryjny / Imei</label>
                  <input type="text" className="form-control" name="imei" id="inputEmail4" placeholder="12312312312" onChange={this.onChange.bind(this)} defaultValue={this.state.getvalue.imei} required/>
                </div>
              </div>
              <div className="form-row">
                <div className="form-group col-md">
                  <label htmlFor="inputEmail4">Kod Blokady</label>
                  <input type="text" className="form-control" name="code" id="inputEmail4" placeholder="Imie" onChange={this.onChange.bind(this)} defaultValue={this.state.getvalue.code} required/>
                </div>
              </div>
              <div className="form-row">
                <div className="form-group col-md">
                  <label htmlFor="exampleFormControlTextarea1">Opis Usterki</label>
                  <textarea className="form-control" name="description" id="exampleFormControlTextarea1" rows="3" onChange={this.onChange.bind(this)} defaultValue={this.state.getvalue.description} required></textarea >
                </div>
              </div>
               <div className="form-row">
                <div className="form-group col-md-6">
                  <label htmlFor="inputEmail4">Koszt naprawy</label>
                  <input type="text" className="form-control" name="cost" id="inputEmail4" placeholder="12312312312" onChange={this.onChange.bind(this)} defaultValue={this.state.getvalue.cost} required/>
                </div>
              </div>
              <div class="form-group">
                  <label for="exampleFormControlSelect1">Forma zapłaty</label>
                  <select class="form-control" name="payment" onChange={this.onChange.bind(this)} id="exampleFormControlSelect1">
                    <option value="0"></option>
                    <option value="1">Gotówka</option>
                    <option value="2">Karta</option>
                    <option value="3">Reklamacja/Gwarancja</option>
                  </select>
                </div>
                <div className="btn-toolbar ">
                  <div className="btn-group mr-2 ">
                    
                    <a className="btn btn-primary" target="_blank" rel="noopener noreferrer" href={this.getPDFlink()}>Wygeneruj PDF</a>
                    </div>
                  <div className="btn-group mr-2 ">
                    <button className="btn btn-primary" data-dismiss="modal" onClick={this.updateRepair.bind(this)}>Zakończ</button>
                  </div>
                </div>

             </form>
          </div>
        </div>
      )
    }
 }
    renderRepairs(){
     
      return this.state.datatest.map((data, key) =>  {
            return (
            <tr key={key}>
              <td>
               {key+1}
              </td>
            <td>
               {data.number}
            </td>
            <td>
              {data.name}
            </td>
            <td>
              {data.surname}
            </td>
            <td>
              {data.device}
            </td>
            <td>
              {data.imei}
            </td>
            <td>
              {data.code}
            </td>
            <td>
              {data.description}
            </td>
            <td>
              {data.cost}
            </td>
          <td>
          {data.status ? (
            <span class="badge badge-success">Zakonczone</span>
              ) : (
            <span class="badge badge-warning">Trwa</span>
            )}
          </td>
          <td>
            <div className="btn-toolbar ">
              <div className="btn-group mr-2 ">
                <button type="submit" className="btn btn-primary" data-toggle="modal" data-target="#exampleModalLong" onClick={this.loadModal.bind(this)} id={key}>
                    Edycja
                </button>
              </div>
              <div className="btn-group mr-2 ">
                <button type="submit" className="btn btn-primary" onClick={this.deleteRepair.bind(this)} id={data.id_r}>
                    Usuń
                </button>
                </div>
            </div>
          </td>
        </tr>
            );
        })

    }




  render() {
     var modal = [];
      modal.push(<div className="modal fade bd-example-modal-lg" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
      <div className="modal-dialog modal-lg" role="document">
        <div className="modal-content">
          <div className="modal-header">
          <h5 className="modal-title">naprawa</h5>
            <button type="button" className="close" data-dismiss="modal" aria-label="Close" onClick={this.closeModal.bind(this)}>
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div className="modal-body">
           { this.renderModal() }
          </div>
        </div>
      </div>
    </div>);


    return (

      <div> 
        <Nav link="Logout" />       
          <div className="container">
          <form className="form-inline" onSubmit={this.searchRepair.bind(this)}>
            <div className="form-group mr-3 mb-2">
              <input type="text" className="form-control" name="number_repairs"  id="name" placeholder="Wpisz numer naprawy" onChange={this.onChange.bind(this)} required />            
            </div>
            <button type="submit" className="btn btn-primary mb-2 btn-dark ">
              Sprawdz
            </button>
          </form>
          <table className="table table-striped table-bordered table-sm">
            <thead className="thead-light">
              <tr>
                <th scope="col">Np.</th>
                <th scope="col">Numer naprawy</th>
                <th scope="col">Imie</th>
                <th scope="col">Nazwisko</th>
                <th scope="col">Nazwa urządzenia</th>
                <th scope="col">Imei</th>
                <th scope="col">Kod blokady</th>
                <th scope="col">Opis</th>
                <th scope="col">Koszt</th>
                <th scope="col">Status</th>
                <th scope="col">Opcje</th>
              </tr>
            </thead>
          <tbody>
          { this.renderRepairs() }
          </tbody>
          </table>
            <div>
            <div class="d-flex justify-content-center">
              <Pagination
                activePage={this.state.activePage}
                itemsCountPerPage={this.state.itemsCountPerPage}
                totalItemsCount={this.state.totalItemsCount}
                pageRangeDisplayed={this.state.pageRangeDisplayed}
                onChange={this.handlePageChange}
                itemClass='page-item'
                linkClass='page-link'

              />
            </div>
            </div>
          </div> 
          {modal}
        </div>







      )
  }

}

export default List