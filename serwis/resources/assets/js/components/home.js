import React, { Component } from 'react'
import ReactDom from'react-dom'
import Nav from './navbar'





class Home extends Component {

  constructor(){
    super();
    this.state = {
      name: '',
      surname: '',
      phone_number: '',
      email: '',
      device: '',
      imei: '',
      description: '',
      code: '',
      cost: '',
    }
  }

addRepair(e){
        e.preventDefault();
        const {name , surname, phone_number, email, device, imei, description, code, cost} = this.state ;
        console.log("dane:" + name + surname + phone_number + email);
        axios.post('api/repairs', {
            name, 
            surname,
            phone_number,
            email,
            device,
            imei,
            description,
            code,
            cost
          })
          .then(response=> {
            this.setState({err: false});
            this.props.history.push("home");
            
          })
          .catch(error=> {
            this.refs.name.value="";
            this.setState({err: true});
          });
     }


     onChange(e){
        const {name, value} = e.target;

        console.log({[name]: value});
        this.setState({[name]: value});
     }



  render() {
    return (
      <div> 
        <Nav link="Logout" />       
          <div className="container text-center title">
            <h1>Nowa Naprawa</h1>
          </div>
          
          <div className="container">
            <form role="form" method="POST">
              <div className="form-row">
                <div className="form-group col-md-6">

                  <label htmlFor="name">Imie</label>
                  <input type="text" className="form-control" name="name"  id="name" placeholder="Imie" onChange={this.onChange.bind(this)} required />
                </div>
                <div className="form-group col-md-6">
                  <label htmlFor="surname">Nazwisko</label>
                  <input type="text" className="form-control" name="surname"id="surname" placeholder="Nazwisko" onChange={this.onChange.bind(this)} required />
                </div>
              </div>
              <div className="form-row">
                <div className="form-group col-md-6">
                  <label htmlFor="inputEmail4">Numer telefonu</label>
                  <input type="text" className="form-control" name="phone_number" id="phone_number" placeholder="500300330" onChange={this.onChange.bind(this)} required />
                </div>
                <div className="form-group col-md-6">
                  <label htmlFor="email">Email</label>
                  <input type="text" className="form-control" name="email" id="email" placeholder="example@emial.com" onChange={this.onChange.bind(this)} required />
                </div>
              </div>

              <div className="form-row">
                <div className="form-group col-md">
                  <label htmlFor="inputEmail4">Nazwa urządzenia</label>
                  <input type="text" className="form-control"  name="device" id="inputEmail4" placeholder="Nazwa" onChange={this.onChange.bind(this)} required/>
                </div>
              </div>
              <div className="form-row">
                <div className="form-group col-md">
                  <label htmlFor="inputEmail4">Numer seryjny / Imei</label>
                  <input type="text" className="form-control" name="imei" id="inputEmail4" placeholder="Imei" onChange={this.onChange.bind(this)} required/>
                </div>
                <div className="form-group col-md">
                  <label htmlFor="inputEmail4">Kod Blokady</label>
                  <input type="text" className="form-control" name="code" id="inputEmail4" placeholder="Kod Blokady" onChange={this.onChange.bind(this)} required/>
                </div>
              </div>
              <div className="form-row">
                <div className="form-group col-md">
                  <label htmlFor="exampleFormControlTextarea1">Opis Usterki</label>
                  <textarea className="form-control" name="description" id="exampleFormControlTextarea1" rows="3" onChange={this.onChange.bind(this)} required></textarea >
                </div>
              </div>
               <div className="form-row">
                <div className="form-group col-md-6">
                  <label htmlFor="inputEmail4">Koszt naprawy</label>
                  <input type="text" className="form-control" name="cost" id="inputEmail4" placeholder="cena" onChange={this.onChange.bind(this)} required/>
                </div>
              </div>
                <div className="btn-group mr-2">
                  <button className="btn btn-primary" onClick={this.addRepair.bind(this)}>Dodaj Naprawe</button>
                </div>
               
          </form>
        </div>   
      </div>

    )
  }
}


export default (Home)