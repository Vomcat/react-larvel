import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import axios from 'axios'
import { withRouter } from 'react-router-dom'


class Nav extends Component {

  constructor(props){
      super(props);
      this.state = {is_admin: null}

  } 
  

  componentDidMount(){
      axios.get('/api/users/check').then(response=>{
        this.setState({
          is_admin: response.data.is_admin
         
          })  
       
      }).catch(error => {
        console.log(error)
      })
    }

  logout(e){
       e.preventDefault();  
       axios.post('api/logout')
          .then(response=> {

            this.props.history.push('/');
          })
          .catch(error=> {
            console.log(error);
          });
  }
  
  handleClick(e){

    e.preventDefault();
    this.props.history.push('/');

  }




renderAdmin(){

  if(this.state.is_admin)
  {

    return(
            <li className="nav-item dropdown">
              <a className="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Panel admina
              </a>
              <div className="dropdown-menu" aria-labelledby="navbarDropdown">
                <Link to="/users" className="dropdown-item">Statystyki </Link>
                <Link to="/users" className="dropdown-item">Użytkownicy</Link>
                <Link to="/products" className="dropdown-item">Towary</Link>
              </div>
            </li>
    );
  }
}


  render() {

    if (this.props.link) {
      return (
        <header className="navbar navbar-expand-lg navbar-dark bg-dark mb-4">
          <div className="container ">
            
              <ul className="nav navbar-nav navbar-right">
                <li className="nav-item">
                  <Link className="nav-link" to="/home">Strona Główna</Link>
                </li>
                <li className="nav-item">
                  <Link className="nav-link" to="/list">Naprawy</Link>
                </li>
                <li className="nav-item">
                  <Link className="nav-link" to="/shop">Sprzedaż</Link>
                </li>
              
                 {this.renderAdmin()}
              </ul>
              <ul className="nav navbar-nav navbar-right">
                 <a className="nav-link" href="#" onClick={this.logout.bind(this)}>Logout</a>  
</ul>
               
          </div>
        </header>
        )
    }

    return (
        <header className="navbar navbar-expand-lg navbar-dark bg-dark mb-4">
          <div>
           
              <ul className="nav navbar-nav navbar-right">
                <li className="nav-item">
                <Link className="nav-link" to="/login">Login</Link>
                </li>
                <li className="nav-item">
                  <Link className="nav-link" to="/">Strona Główna</Link>
                </li>
              </ul>
          </div>
        </header>
      
    )
  }

}

export default withRouter(Nav)