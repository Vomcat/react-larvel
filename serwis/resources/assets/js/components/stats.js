import React, { Component } from 'react'
import Nav from './navbar'
import axios from 'axios'
import Pagination from 'react-js-pagination'






class Stats extends Component {

  constructor(){
    super();
    this.state = {
      datatest:[],
       activePage:1,
       itemsCountPerPage:1,
       totalItemsCount:1,
       

    }
  }


componentDidMount()
{
  axios.get('/api/repairs/index')
  .then(response=>{
    this.setState(datatest:response.data);
  })
}
handlePageChange(pageNumber) {
    console.log(`active page is ${pageNumber}`);
    this.setState({activePage: pageNumber});
  }

 renderRepairs(){
     
      return this.state.datatest.map((data, key) =>  {
            return (
            <tr key={key}>
              <td>
               {key+1}
              </td>
            <td>
               {data.number}
            </td>
            <td>
              {data.name}
            </td>
            <td>
              {data.surname}
            </td>
            <td>
              {data.device}
            </td>
            <td>
              {data.imei}
            </td>
            <td>
              {data.code}
            </td>
            <td>
              {data.description}
            </td>
            <td>
              {data.cost}
            </td>
          <td>
          {data.status ? (
            <span class="badge badge-success">Zakonczone</span>
              ) : (
            <span class="badge badge-warning">Trwa</span>
            )}
          </td>
          <td>
            <div className="btn-toolbar ">
              <div className="btn-group mr-2 ">
                <button type="submit" className="btn btn-primary" data-toggle="modal" data-target="#exampleModalLong" onClick={this.loadModal.bind(this)} id={key}>
                    Edycja
                </button>
              </div>
              <div className="btn-group mr-2 ">
                <button type="submit" className="btn btn-primary" onClick={this.deleteRepair.bind(this)} id={data.id_r}>
                    Usuń
                </button>
                </div>
            </div>
          </td>
        </tr>
            );
        })

    }




  render() {
    
  

      <div> 
        <Nav link="Logout" />       
          <div className="container">
          <form className="form-inline" onSubmit={this.searchRepair.bind(this)}>
            <div className="form-group mr-3 mb-2">
              <input type="text" className="form-control" name="number_repairs"  id="name" placeholder="Wpisz numer naprawy" onChange={this.onChange.bind(this)} required />            
            </div>
            <button type="submit" className="btn btn-primary mb-2 btn-dark ">
              Sprawdz
            </button>
          </form>
          <table className="table table-striped table-bordered table-sm">
            <thead className="thead-light">
              <tr>
                <th scope="col">Np.</th>
                <th scope="col">Numer naprawy</th>
                <th scope="col">Imie</th>
                <th scope="col">Nazwisko</th>
                <th scope="col">Nazwa urządzenia</th>
                <th scope="col">Imei</th>
                <th scope="col">Kod blokady</th>
                <th scope="col">Opis</th>
                <th scope="col">Koszt</th>
                <th scope="col">Status</th>
                <th scope="col">Opcje</th>
              </tr>
            </thead>
          <tbody>
          { this.renderRepairs() }
          </tbody>
          </table>
            <div>
              <Pagination
                activePage={this.state.activePage}
                itemsCountPerPage={10}
                totalItemsCount={450}
                pageRangeDisplayed={5}
                onChange={this.handlePageChange}
              />
            </div>
          </div> 
         
        </div>







 
  }

}

export default Stats