import React, { Component } from 'react'
import { BrowserRouter } from "react-router-dom"
import Nav from './navbar'


class Index extends Component {

  constructor(){

    super();
    this.state = {
       status,
       show : false,
       n_error : false
    }
  }

  onSubmit(e){
        e.preventDefault();
        const {number_repairs} = this.state ;
        
        axios.get('api/repairs/'+ number_repairs)
          .then(response=> {
            this.setState({ status: response.data[0].status, show : true}) 
            console.log(this.state.status);             
          })
          .catch(error=> {
             this.setState({n_error: true, show : false}) 
            this.setState({err: true});
          });
     }
     onChange(e){
        const {name, value} = e.target;
        console.log(value);
        this.setState({[name]: value});
     }

       
  renderStatus(){
    

    if(this.state.show){
      return(
        this.state.status ? (
      <span class="badge badge-success">Zakonczone</span>
      ) : (
        <span class="badge badge-warning">Trwa</span>
          )
        )
    }
    if(this.state.n_error){
      return(<span class="badge badge-danger">Nie znaleziono</span>)
      
    }
  }


  render() {
    return (
       <div> 
          <Nav />       
          <div className="container">
            <h1 className="text-center ">Sprawdz status naprawy </h1>
            <form className="form-horizontal" role="form" onSubmit= {this.onSubmit.bind(this)}>

              <div className="form-group">
                <div className="col-md-6 col-md-offset-4 mx-auto">
                  <input type="text" className="form-control" name="number_repairs"  id="name" placeholder="Wpisz numer naprawy" onChange={this.onChange.bind(this)} required />
                </div>
              </div>
              <div className="form-group form-row text-center">
                <div className="col 12">
                  <button type="submit" className="btn btn-primary btn btn-dark ">
                    Sprawdz
                  </button>
                </div>
              </div>
            </form>
          </div>
            {this.renderStatus()}
      </div>
         
    )
  }

}

export default Index