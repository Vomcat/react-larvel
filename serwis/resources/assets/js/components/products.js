import React, { Component } from 'react'
import Nav from './navbar'
import axios from 'axios'






class Products extends Component {

  constructor(props){
    super(props);
    this.state = {products:[], getvalue: null, m_edit: false, m_add: false};
  }

  componentWillMount(){

      axios.get('/api/products').then(response=>{
        this.setState({
        products: response.data
          })  
      }).catch(error => {
        console.log(error)
      })
    }

    updateProduct(e){
        e.preventDefault();
        const {name , amount, price_r, price_d} = this.state ;
        axios.put('api/products/updateproduct/' + this.state.getvalue.id_r, {
            name, 
            amount,
            price_r,
            price_d,

          })
          .then(response=> {
            this.setState({err: false});
            this.closeModal();
            this.componentWillMount();

            
          })
          .catch(error=> {
            this.refs.name.value="";
            this.setState({err: true});
          });
     }

   addProduct(e){
        e.preventDefault();
        const {name , amount, price_r,price_d, p_code} = this.state ;
        axios.post('api/products', {
            name, 
            amount,
            price_r,
            price_d,
            p_code,
          })
          .then(response=> {
            this.setState({err: false});
            this.props.history.push("home");
            
          })
          .catch(error=> {
            this.refs.name.value="";
            this.setState({err: true});
          });
     }  

  deleteProduct(e){
        const {id} = this.state ;
        axios.get('api/products/delete/' + e.target.id).then(response=> {
            this.setState({err: false});
            this.componentWillMount();
            
          })
          .catch(error=> {
            this.setState({err: true});
          });
     }


  onChange(e){
    const {name, value} = e.target;
    
    this.setState({[name]: value});

  }
  loadModal(e){
    if(e.target.id == 'addnew'){
      this.setState({m_add: true});
    }
    else{
      this.setState({m_edit: true, getvalue: this.state.products[e.target.id]});
    }
    
    this.renderModal();
    console.log("test");
  }

  closeModal(){
    this.setState({m_edit: false, m_add: false});
  }


  renderModal(){
    console.log(this.state.m_edit);
    if(this.state.m_edit)
    {
    return(
           <form>
             <div className="form-group">
               <label for="exampleInputEmail1">Nazwa towaru</label>
               <input type="text" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email" name="name" onChange={this.onChange.bind(this)} defaultValue={this.state.getvalue.name}/>
             </div>
             <div className="form-group">
               <label for="exampleInputPassword1">Cena detaliczna</label>
               <input type="text" className="form-control" id="exampleInputPassword1" placeholder="Cena" name="price_r" onChange={this.onChange.bind(this)} defaultValue={this.state.getvalue.price_r}></input>
             </div>
             <div className="form-group">
               <label for="exampleInputEmail1">Cena hurt</label>
               <input type="text" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email" name="name" onChange={this.onChange.bind(this)} defaultValue={this.state.getvalue.price_d}/>
             </div>
             <div className="form-group">
               <label for="exampleInputEmail1">Ilość</label>
               <input type="text" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email" name="name" onChange={this.onChange.bind(this)} defaultValue={this.state.getvalue.amount}/>
             </div>
             <div className="form-group">
               <label for="exampleInputEmail1">Ilość</label>
               <input type="text" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email" name="name" onChange={this.onChange.bind(this)} defaultValue={this.state.getvalue.p_code}/>
             </div>




             <button type="submit" className="btn btn-primary" data-dismiss="modal" aria-label="Close" onClick={this.updateProduct.bind(this)}>Zapisz</button>
           </form>
      )
  }
  else if(this.state.m_add){
    return(
               <form>
      <div className="form-group">
      <label for="exampleInputEmail1">Nazwa towaru</label>
    <input type="text" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email" name="name" onChange={this.onChange.bind(this)}/>
  </div>
  <div className="form-group">
    <label for="exampleInputPassword1">Cena detaliczna</label>
    <input type="text" className="form-control" id="exampleInputPassword1" placeholder="Password" name="price_r" onChange={this.onChange.bind(this)}/>
  </div>
  <div className="form-group">
    <label for="exampleInputPassword1">Cena Hurt</label>
    <input type="text" className="form-control" id="exampleInputPassword1" placeholder="Password" name="price_d" onChange={this.onChange.bind(this)}/>
  </div>
  <div className="form-group">
    <label for="exampleInputPassword1">Ilość</label>
    <input type="text" className="form-control" id="exampleInputPassword1" placeholder="Password" name="amount" onChange={this.onChange.bind(this)}/>
  </div>
  <div className="form-group">
    <label for="exampleInputPassword1">Ilość</label>
    <input type="text" className="form-control" id="exampleInputPassword1" placeholder="Password" name="p_code" onChange={this.onChange.bind(this)}/>
  </div>





  <button type="submit" className="btn btn-primary" data-dismiss="modal" aria-label="Close" onClick={this.updateProduct.bind(this)}>Zapisz</button>
</form>
)
  }

 }


        renderProducts(){
     

            return this.state.products.map((data, key) =>  {

            
            return (

          <tr key={key}>
          <td>
            {key+1}
          </td>
          <td>
            {data.name}
          </td>
          <td>
            {data.price_r} zł
          </td>
          <td>
            {data.price_d}
          </td>
          <td>
            {data.amount}
          </td>
          <td>
            {data.p_code}
          </td>
          <td>
            <button type="submit" className="btn btn-primary" onClick={this.deleteProduct.bind(this)} id={key}>
                    Usuń
                </button>
                <button className="btn btn-primary" data-toggle="modal" data-target="#editmodal" onClick={this.loadModal.bind(this)} id={key}>
                    Edytuj
                </button>
          </td>
          </tr>
            );
        })
     
    };




  render() {
    var modal = [];
      modal.push(<div className="modal fade bd-example-modal-lg" id="editmodal" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
      <div className="modal-dialog modal-lg" role="document">
        <div className="modal-content">
          <div className="modal-header">
          <h5 className="modal-title">naprawa</h5>
            <button type="button" className="close" data-dismiss="modal" aria-label="Close" onClick={this.closeModal.bind(this)}>
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div className="modal-body">
           { this.renderModal() }
          </div>
        </div>
      </div>
    </div>);
    return (
      <div> 
        <Nav link="Logout" />       
          <div className="container">
          <button className="btn btn-primary" data-toggle="modal" data-target="#editmodal" onClick={this.loadModal.bind(this)} id="addnew">
                    dodaj
                </button>
            <table className="table table-borderless">
              <thead className="thead-light">
                <tr>
                  <th scope="col">Np</th>
                  <th scope="col">Produkt</th>
                  <th scope="col">Cena detaliczna</th>
                  <th scope="col">Cena hurt</th>
                  <th scope="col">Ilość</th>
                  <th scope="col">Kod</th>
                  <th scope="col">Opcje</th>
                </tr>
              </thead>
              <tbody>
                { this.renderProducts() }
                
              </tbody>
            </table>
            
          </div> 
          {modal}
        </div>
      )
  }

}

export default Products