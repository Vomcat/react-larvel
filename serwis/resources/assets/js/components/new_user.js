import React, { Component } from 'react'
import Nav from './navbar'
import axios from 'axios'




class New_user extends Component {

  constructor(){
    super();
    this.state = {datatest:[], dataid: null, m_newusser: false, m_pass: false}


  }




addNewUser(e){
        e.preventDefault();
        const {name , password} = this.state ;
        axios.post('api/users', {
            name, 
            password,
          })
          .then(response=> {
            this.setState({err: false});
            this.closeModal();
            this.componentDidMount();
            
            
          })
          .catch(error=> {
            this.refs.name.value="";
            this.setState({err: true});
          });
     }

deleteUser(e){
        const {id} = this.state ;
        axios.get('api/users/delete/' + e.target.id).then(response=> {
            this.setState({err: false});
            this.componentDidMount();
          
          })
          .catch(error=> {
            this.setState({err: true});
          });
     }

componentDidMount(){
      axios.get('/api/users').then(response=>{
        this.setState({
          datatest: response.data
          
          })  
      }).catch(error => {
        console.log(error)
      })
    }

     onChange(e){
        const {name, value} = e.target;
        console.log({[name]: value});
        this.setState({[name]: value});
     }

updatePassword(e){
        e.preventDefault();
        const {password} = this.state;
        axios.put('api/users/newpassword/' + this.state.dataid, {
            password
          })
          .then(response=> {
            this.setState({err: false});
            this.closeModal();
            
          })
          .catch(error=> {
            this.refs.name.value="";
            this.setState({err: true});
          });
     }

loadModal(e){
  console.log(e.target);
  if(e.target.name == 'newuser'){
    this.setState({m_newusser: true});
    this.renderModal();

  }
  if(e.target.name == 'changepassword')
  {
    this.setState({dataid: e.target.id, m_pass: true});
    this.renderModal();
  }

}
closeModal(){

    this.setState({m_newusser: false, m_pass: false});
    this.renderModal();


}


 renderModal(){

  if(this.state.m_newusser){
    return(
        <form>
  <div className="form-group">
    <label for="exampleInputEmail1">Nazwa użytkownika</label>
    <input type="text" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email" name="name" onChange={this.onChange.bind(this)} />
  </div>
  <div className="form-group">
    <label for="exampleInputPassword1">Hasło</label>
    <input type="password" className="form-control" id="exampleInputPassword1" placeholder="Password" name="password" onChange={this.onChange.bind(this)}></input>
  </div>
  <button type="submit" className="btn btn-primary" data-dismiss="modal" aria-label="Close" onClick={this.addNewUser.bind(this)}>Zapisz</button>
</form>
      )
  }
  else if(this.state.m_pass){
    return(
            <form>
  <div className="form-group">
    <label for="exampleInputPassword1">Nowe hasło</label>
    <input type="password" className="form-control" id="exampleInputPassword1" placeholder="Password" name="password" onChange={this.onChange.bind(this)}></input>
  </div>
  <button type="submit" className="btn btn-primary" data-dismiss="modal" aria-label="Close" onClick={this.updatePassword.bind(this)}>Zapisz</button>
</form>
  )}

 }



    renderUsers(){
     
      return this.state.datatest.map((data, key) =>  {

            
            return (
           <tr key={key}>
          <td>
            {key+1}
          </td>
          <td>
            {data.name}
          </td>
          <td>
            {data.created_at}
          </td>
          <td>
            <div className="col-md-8 col-md-offset-10">
                 
                <button type="submit" className="btn btn-primary" data-toggle="modal" data-target="#ModalUsers"  name="changepassword" onClick={this.loadModal.bind(this)} id={data.id}>
                    Zmień haslo
                </button>
                <button type="submit" className="btn btn-primary" onClick={this.deleteUser.bind(this)} id={data.id}>
                    Usuń
                </button>

            </div>
          </td>

        </tr>
            );
        })

    }




  render() {
     var modal = [];
      modal.push(<div className="modal fade bd-example-modal-lg" id="ModalUsers" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true" >
      <div className="modal-dialog" role="document">
        <div className="modal-content">
          <div className="modal-header">
          <h5 className="modal-title">naprawa</h5>
            <button type="button" className="close"  data-dismiss="modal" aria-label="Close" onClick={this.closeModal.bind(this)}>
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div className="modal-body">
           {this.renderModal()}
          </div>
        </div>
      </div>
    </div>);


    return (
      <div> 
        <Nav link="Logout" />       
          <div className="container">
          <button type="submit" className="btn btn-primary" data-toggle="modal" data-target="#ModalUsers" name="newuser" onClick={this.loadModal.bind(this)}>
                    Dodaj nowego użytkowanika
                </button>
            <table className="table">
              <thead className="thead-light">
                <tr>
                  <th scope="col">Rola</th>
                  <th scope="col">Data dodania</th>
                  <th scope="col">Opcje</th>
                  

                </tr>
              </thead>
              <tbody>{
                
                this.state.datatest.map(data =>  {
              return (

            
           <tr >
         
          <td>
            {data.name}
          </td>
          <td>
            {data.created_at}
          </td>
          <td>
            <div className="col-md-8 col-md-offset-10">
                 
                <button type="submit" className="btn btn-primary" data-toggle="modal" data-target="#ModalUsers"  name="changepassword" onClick={this.loadModal.bind(this)} id={data.id}>
                    Zmień haslo
                </button>
                <button type="submit" className="btn btn-primary" onClick={this.deleteUser.bind(this)} id={data.id}>
                    Usuń
                </button>

            </div>
          </td>

        </tr>
            )
        })
}
              </tbody>
            </table>
          </div> 
        


{modal}



        </div>






      )
  }

}

export default New_user