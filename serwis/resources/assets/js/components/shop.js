import React, { Component } from 'react'
import Nav from './navbar'
import axios from 'axios'






class Shop extends Component {

  constructor(props){
    super(props);
    this.state = {products:[], shop_products:[], totalamount: 0};
    console.log(this.props.isAuthed);
  }

  componentWillMount(){

      axios.get('/api/products').then(response=>{
        this.setState({
        products: response.data
          
          })  
      }).catch(error => {
        console.log(error)
      })
    }

finishTransaction(e){
        
        const {shop_products} = this.state ;
        axios.post('api/transaction/', {
           shop_products,
           payment: e.target.id
          })
          .then(response=> {
            this.setState({err: false, shop_products: [], totalamount: 0});
           

            
          })
          .catch(error=> {
            this.refs.name.value="";
            this.setState({err: true});
          });
     }

deleteProduct(e){
      this.state.totalamount -= this.state.shop_products[e.target.id].price_r;
       this.state.shop_products.splice(e.target.id, 1);
       
    
      this.setState({shop_products: this.state.shop_products});
     }


  onChange(e){
    const {name, value} = e.target;
    console.log({[name]: value});
    this.setState({[name]: value});

  }

    addProduct(e){
    this.state.shop_products.push(this.state.products[this.state.product]);
    this.state.totalamount += this.state.products[this.state.product].price_r;

     
    this.setState({shop_products: this.state.shop_products});
    console.log(this.state.totalamount);
  }

 



    renderProducts(){
     
      return this.state.products.map((data, key) =>  {

            
            return (

            <option key={key} id={key} value={key}>{data.name} - {data.price_r} zł</option>

            );
        })

    }

        renderShop(){
     
     if(this.state.shop_products){
            return this.state.shop_products.map((data, key) =>  {

            
            return (

          <tr key={key}>
          <td>
            {key+1}
          </td>
          <td>
            {data.name}
          </td>
          <td>
            {data.price_r} zł
          </td>
          <td>
            <button type="submit" className="btn btn-primary" onClick={this.deleteProduct.bind(this)} id={key}>
                    Usuń
                </button>
          </td>
          </tr>
            );
        })
     }
    };




  render() {
    return (
      <div> 
        <Nav link="Logout" />       
          <div className="container">
            <table class="table table-borderless">
              <thead className="thead-light">
                <tr>
                  <th scope="col">Np</th>
                  <th scope="col">Produkt</th>
                  <th scope="col">Cena</th>
                  <th scope="col">Opcje</th>
                </tr>
              </thead>
              <tbody>
                { this.renderShop() }
                <tr>
                <td>
                DO zaplaty {this.state.totalamount} zł
                </td>
                <td>
                  <div className="btn-toolbar ">
                    <div className="btn-group mr-2 ">
                      <button type="submit" className="btn btn-primary" onClick={this.finishTransaction.bind(this)} id="1">
                        Gotówka
                      </button>
                      </div>
                      <div className="btn-group mr-2 ">
                        <button type="submit" className="btn btn-primary" onClick={this.finishTransaction.bind(this)} id="2">
                          Karta
                        </button>
                      </div>
                    </div>
                </td>
                </tr>
              </tbody>
            </table>
            <div class="input-group">
              <select class="custom-select" name="product" onChange={this.onChange.bind(this)} id="inputGroupSelect04">
                <option>Wybierz produkt</option>
                { this.renderProducts() }
              </select>
              <div class="input-group-append">
                <button class="btn btn-outline-secondary" onClick={this.addProduct.bind(this)} type="button">Dodaj</button>
              </div>
            </div>
          </div> 
        </div>
      )
  }

}

export default Shop