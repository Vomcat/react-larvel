<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRepairsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('repairs', function (Blueprint $table) {
            $table->increments('id_r');
            $table->unsignedInteger('customer_id');
            $table->foreign('customer_id')->references('id_c')->on('customers');
            $table->string('device');
            $table->string('imei');
            $table->string('code');
            $table->string('description');
            $table->string('cost');
            $table->integer('payment');
            $table->integer('number');
            $table->integer('status');


            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('repairs');
    }
}
