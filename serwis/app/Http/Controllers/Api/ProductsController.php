<?php

namespace App\Http\Controllers\Api;

use App\Products;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Products::query()->where('status', '1')->get()->all();

        return response()->json($products);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $products = new Products();
        $products->amount = $request->get('amount');
        $products->price_d = $request->get('price_d');
        $products->price_r = $request->get('price_r');
        $products->p_code = $request->get('p_code');
        $products->status = '1';
        $products->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $products = Products::find($id);
            $products->name = $request->get('name') ? $request->get('name') : $products->name;
            $products->amount = $request->get('amount') ? $request->get('amount') : $products->amount;
            $products->price_d = $request->get('price_d') ? $request->get('price_d') : $products->price_d;
            $products->price_r = $request->get('price_r') ? $request->get('price_r') : $products->price_r;
            $products->p_code = $request->get('p_code') ? $request->get('p_code') : $products->p_code;



            $products->save();
              }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

    }

    public function setInactive($id)
    {
        $products = Products::find($id);
        $products->status = '0';
        $products->save();
    }
}
