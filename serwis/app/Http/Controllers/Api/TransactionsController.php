<?php

namespace App\Http\Controllers\Api;

use App\Transactions;
use App\Products;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TransactionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $gen_id_transaction = date('YmdHis');
         foreach($request->get('shop_products') as $shop_products) {
            $transaction = new Transactions;
            $transaction->id_t = $gen_id_transaction;
            $transaction->name = $shop_products['name'];
            $transaction->price_r = $shop_products['price_r'];
            $transaction->p_code = $shop_products['p_code'];
            $transaction->payment = $request->get('payment');
            $transaction->product_id = $shop_products['id'];

            $transaction->save();

            $products = Products::find($shop_products['id']);
            $products->amount -= 1;

            $products->save();
        }
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
