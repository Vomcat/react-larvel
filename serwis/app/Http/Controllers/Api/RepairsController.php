<?php

namespace App\Http\Controllers\Api;

use PDF;
use App\Repairs;
use App\Customers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RepairsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        //$repairs["baza"] = Repairs::query()->join('customers','customers.id_c','=','repairs.customer_id')->orderBy('id_r', 'desc')->paginate(2);
        //Repairs::query()->join('customers','customers.id_c','=','repairs.customer_id')->orderBy('id_r', 'desc')->paginate(2);
        //$repairs=Repairs::all()->join('customers','customers.id_c','=','repairs.customer_id')->orderBy('id_r', 'desc')->paginate(2);
       $repairs=Repairs::join('customers','customers.id_c','=','repairs.customer_id')->orderBy('id_r', 'desc')->paginate(9 ,array('customers.*','repairs.*'));
      
        return $repairs;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $customers = new Customers();
        $customers->name = $request->get('name');
        $customers->surname = $request->get('surname');
        $customers->phone_number = $request->get('phone_number');
        $customers->email = $request->get('email');
        $customers->save();

        $baseValue = 1000;
        $repairs = new Repairs();
        $repairs->device = $request->get('device');
        $repairs->customer_id = $customers->id_c;
        $repairs->imei = $request->get('imei');
        $repairs->code = $request->get('code');
        $repairs->description = $request->get('description');
        $repairs->status = 0;
        $repairs->payment = 0;
        $repairs->cost = $request->get('cost');
        $repairs->number = $baseValue+$customers->id_c;
        
       
        $repairs->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
        $repairs=Repairs::query()->where('number', $id)->get();
      
        return response()->json($repairs);


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
            $repairs = Repairs::find($id);
            $repairs->device = $request->get('device') ? $request->get('device') : $repairs->device;
            $repairs->imei = $request->get('imei') ? $request->get('imei') : $repairs->imei;
            $repairs->code = $request->get('code') ? $request->get('code') : $repairs->code;
            $repairs->description = $request->get('description') ? $request->get('description') : $repairs->description;
            $repairs->cost = $request->get('cost') ? $request->get('cost') : $repairs->cost;
            $repairs->payment = $request->get('payment') ? $request->get('payment') : $repairs->payment;
            $repairs->status = $request->get('payment') > 0 ? 1 : $repairs->status;

            $repairs->save();

            $customers = Customers::find($repairs->customer_id);
            $customers->name = $request->get('name') ? $request->get('name') : $customers->name;
            $customers->surname = $request->get('surname') ? $request->get('surname') : $customers->surname;
            $customers->phone_number = $request->get('phone_number') ? $request->get('phone_number') : $customers->phone_number;
            $customers->email = $request->get('email') ? $request->get('email') : $customers->email;
            $customers->save();
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $repairs = Repairs::find($id);
        $repairs->delete();
    }

    public function status($id)
    {
        $repairs = Repairs::find($id);
        $repairs->status = 1;
        $repairs->save();
    }

    public function pdf_gen($id)
    {

        $data['repairs'] = Repairs::find($id);
        $data['customer'] = Customers::find($data['repairs']->customer_id);
        
        
        $pdf = PDF::loadView('pdf', $data);
        return $pdf->download('naprawa.pdf');
    }

    public function searchRepair($id)
    {
        
       
        $repairs=Repairs::query()->where('number', $id)->join('customers','customers.id_c','=','repairs.customer_id')->orderBy('id_r', 'desc')->get();

        return response()->json($repairs);
    }

    


}
