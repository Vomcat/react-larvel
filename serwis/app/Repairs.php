<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Repairs extends Model
{

	protected $primaryKey = 'id_r';

     protected $fillable = [
        'device', 'imei', 'code', 'description','cost'
    ];

        public function customers()
  {
      return $this->hasMany('App\Customers');
  }
}
