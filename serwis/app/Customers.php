<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customers extends Model
{
	protected $primaryKey = 'id_c';
       protected $fillable = [
        'name', 'surname', 'phone_number', 'email'
    ];


}
